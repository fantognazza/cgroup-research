![build status](https://gitlab.com/fantognazza/cgroup-research/badges/master/build.svg)

# Abstract

This project is to be considered as a simple report on linux control group.

As the researched subject is yet being developed, informations contained in this repo are updated on the date of publication.

Code analysis is made on Tejun's repository, in particular on “review-cgroup2-cpu-on-v4” branch witch is based on the Linux kernel 4.13.

References are published at the end of the document in the bibliography section.
All links are valid at the time of publication, but in the future they may stop to work.

# Files description

* **cgroup-research.lyx**:        source file of document to be used with Lyx editor

* **cgroup-research.tex**:        LaTeX source exported from Lyx editor. It's used by gitlab-ci to generate PDFs

* **ftrace_function_graph**:      contains full and cleaned ftrace logs with generating scripts

* **cpuset_patch**:               contains a patch to enable cpuset controller in default hierarchy

# Download the PDF

[cgroup-research.pdf](https://gitlab.com/fantognazza/cgroup-research/-/jobs/artifacts/master/raw/cgroup-research.pdf?job=compile_pdf)
