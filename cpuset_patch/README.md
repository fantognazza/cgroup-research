# CPUSET controller patch

This patch enables cpuset subsystem in default unified hierarchy offered by cgroup v2, so controller's name is listed in cgroup.controllers core interface file. It's still compatible with legacy hierarchy of cgroup v1 filesystem.

# How to apply

Move to linux source path, go to kernel/cgroup and then apply patch with
```
patch -p1 < path/to/cpuset.patch
```

 
